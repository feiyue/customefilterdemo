//
//  AppDelegate.h
//  TestGPUImageFundationOC
//
//  Created by admin on 2020/1/9.
//  Copyright © 2020 AppTime. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end


//
//  ViewController.m
//  TestGPUImageFundationOC
//
//  Created by admin on 2020/1/9.
//  Copyright © 2020 AppTime. All rights reserved.
//

#import "ViewController.h"
#import <GPUImage/GPUImage.h>

#import "GPUImageHorizontalFlipFilter.h"
#import "GPUImageRotate90ClockwiseFilter.h"
#import "GPUImageZoomFilter.h"
#import "GPUImageBigEyeFilter.h"
#import "GPUImageVerticesHorizontalFlipFilter.h"

#define Width_Screen [[UIScreen mainScreen] bounds].size.width
#define Height_Screen [[UIScreen mainScreen] bounds].size.height

@interface ViewController ()

@property (nonatomic, strong) GPUImagePicture *imagePicture;
@property (nonatomic, strong) GPUImageView *imageView;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UIButton *filterBtn;

@property (nonatomic, strong) GPUImageFilter *filter;

@property (nonatomic, strong) GPUImageSaturationFilter *saturationFilter;

//GPUImageSphereRefractionFilter
@property (nonatomic, strong) GPUImageSphereRefractionFilter *sphereRefractionFilter;
@property (nonatomic, strong) GPUImagePinchDistortionFilter *pinchFilter;

//Custom Filter
@property (nonatomic, strong) GPUImageVerticesHorizontalFlipFilter *customFilter;
//@property (nonatomic, strong) GPUImageRotate90ClockwiseFilter *customFilter;
//@property (nonatomic, strong) GPUImageZoomFilter *customFilter;
//@property (nonatomic, strong) GPUImageBigEyeFilter *customFilter;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor yellowColor];
    
    [self.view addSubview:self.imageView];
    [self.view addSubview:self.slider];
    [self.view addSubview:self.filterBtn];
    
    _filter = [GPUImageSketchFilter new];
    _saturationFilter = [GPUImageSaturationFilter new];
    _saturationFilter.saturation = 2.0;
    
    //测试 3
    _sphereRefractionFilter = [GPUImageSphereRefractionFilter new];
    _sphereRefractionFilter.radius = 10.0;
    _sphereRefractionFilter.refractiveIndex = 30;
    
    //测试 4
    _pinchFilter = [GPUImagePinchDistortionFilter new];
    //_pinchFilter.center = CGPointMake(0, 1.0);
    _pinchFilter.radius = 0.5;
    //_pinchFilter.scale = 1.5;
    
#define mark -
    //Custom
    _customFilter = [GPUImageVerticesHorizontalFlipFilter new];
    GLfloat meshVertices[8] = {1.0, -1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0};
    [_customFilter setVertices:meshVertices];
    
    [self.imagePicture addTarget:_customFilter];
    [_customFilter addTarget:self.imageView];
    
    [self.imagePicture processImage];
}

#pragma mark -

- (void)clickChangeFilterButtonWithEvent:(UIButton *)sender {
    [self.imagePicture removeAllTargets];
    
    //[self.imagePicture addTarget:self.saturationFilter];
    [self.imagePicture addTarget:self.sphereRefractionFilter];
    [self.saturationFilter addTarget:self.imageView];
    [self.imagePicture processImage];
}

- (void)clickChangeSliderWithEvent:(UISlider *)slider {
    //_customFilter.scale = slider.value;
    
    [self.imagePicture processImage];
}


#pragma mark -

- (GPUImagePicture *)imagePicture {
    if (!_imagePicture) {
        _imagePicture = [[GPUImagePicture alloc] initWithImage:[UIImage imageNamed:@"known.jpg"]];
        
    }
    
    return _imagePicture;
}

- (GPUImageView *)imageView {
    if (!_imageView) {
        _imageView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 0, Width_Screen, (Height_Screen/4*3))];
        _imageView.backgroundColor = [UIColor grayColor];
        
    }
    
    return _imageView;
}

- (UISlider *)slider {
    if (!_slider) {
        _slider = [[UISlider alloc] initWithFrame:CGRectMake(0, 0, Width_Screen - 80, 40)];
        _slider.center = CGPointMake(Width_Screen/2, (Height_Screen/4*3) + 30);
        _slider.maximumValue = 4;
        _slider.minimumValue = 0;
        [_slider addTarget:self action:@selector(clickChangeSliderWithEvent:) forControlEvents:UIControlEventValueChanged];
    }
    
    return _slider;
}

- (UIButton *)filterBtn {
    if (!_filterBtn) {
        _filterBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Width_Screen / 2, 80)];
        _filterBtn.center = CGPointMake(Width_Screen / 2, Height_Screen - 60);
        _filterBtn.backgroundColor = [UIColor redColor];
        [_filterBtn setTitle:@"Change Filter" forState:UIControlStateNormal];
        [_filterBtn addTarget:self action:@selector(clickChangeFilterButtonWithEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _filterBtn;
}


@end

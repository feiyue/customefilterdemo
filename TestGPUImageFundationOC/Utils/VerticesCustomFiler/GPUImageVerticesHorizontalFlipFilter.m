//
//  GPUImageVerticesHorizontalFlipFilter.m
//  TestGPUImageFundationOC
//
//  Created by admin on 2020/1/10.
//  Copyright © 2020 AppTime. All rights reserved.
//

#import "GPUImageVerticesHorizontalFlipFilter.h"

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE

NSString *const kGPUImageVerticesHorizontalFilpFragmentShadering = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 void main()
{
    gl_FragColor = texture2D(inputImageTexture, textureCoordinate);
}
);

#else

NSString *const kGPUImageVerticesHorizontalFilpFragmentShadering = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 void main()
{
    gl_FragColor = texture2D(inputImageTexture, textureCoordinate);
}
);

#endif


#pragma mark -

@interface GPUImageVerticesHorizontalFlipFilter () {
    GLfloat kVertics[8];
}

@end

@implementation GPUImageVerticesHorizontalFlipFilter

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageVerticesHorizontalFilpFragmentShadering])) {
        return nil;
    }
    
    return self;
}

- (void)setVertices:(const GLfloat *)vertices {
    kVertics[0] = vertices[0];
    kVertics[1] = vertices[1];
    kVertics[2] = vertices[2];
    kVertics[3] = vertices[3];
    
    kVertics[4] = vertices[4];
    kVertics[5] = vertices[5];
    kVertics[6] = vertices[6];
    kVertics[7] = vertices[7];
}


#pragma mark - Override Rendering

- (void)renderToTextureWithVertices:(const GLfloat *)vertices textureCoordinates:(const GLfloat *)textureCoordinates {
    if (self.preventRendering) {
        [firstInputFramebuffer unlock];
        return;
    }
    
    [GPUImageContext setActiveShaderProgram:filterProgram];

    outputFramebuffer = [[GPUImageContext sharedFramebufferCache] fetchFramebufferForSize:[self sizeOfFBO] textureOptions:self.outputTextureOptions onlyTexture:NO];
    [outputFramebuffer activateFramebuffer];
    if (usingNextFrameForImageCapture)
    {
        [outputFramebuffer lock];
    }

    [self setUniformsForProgramAtIndex:0];
    
    glClearColor(backgroundColorRed, backgroundColorGreen, backgroundColorBlue, backgroundColorAlpha);
    glClear(GL_COLOR_BUFFER_BIT);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, [firstInputFramebuffer texture]);
    
    glUniform1i(filterInputTextureUniform, 2);

    //GLfloat meshVertices[8] = {1.0, -1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0};
    glVertexAttribPointer(filterPositionAttribute, 2, GL_FLOAT, 0, 0, &kVertics);
    glVertexAttribPointer(filterTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, textureCoordinates);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    [firstInputFramebuffer unlock];
    
    if (usingNextFrameForImageCapture)
    {
        dispatch_semaphore_signal(imageCaptureSemaphore);
    }
    
}

@end

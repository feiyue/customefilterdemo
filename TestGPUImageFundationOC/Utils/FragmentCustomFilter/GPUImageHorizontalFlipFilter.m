//
//  GPUImageHorizontalFlipFilter.m
//  TestGPUImageFundationOC
//
//  Created by admin on 2020/1/10.
//  Copyright © 2020 AppTime. All rights reserved.
//

#import "GPUImageHorizontalFlipFilter.h"


#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE

NSString *const kGPUImageHorizontalFlipFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 void main()
 {
    highp vec2 textureCoordinateToUse = vec2(1.0 - textureCoordinate.x, textureCoordinate.y);
    
    gl_FragColor = texture2D(inputImageTexture, textureCoordinateToUse);
 }
 );

#else

NSString *const kGPUImageHorizontalFlipFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 void main()
 {
    highp vec2 textureCoordinateToUse = vec2(1.0 - textureCoordinate.x, textureCoordinate.y);
    
    gl_FragColor = texture2D(inputImageTexture, textureCoordinateToUse);
 }
 );

#endif


@interface GPUImageHorizontalFlipFilter ()

@end


@implementation GPUImageHorizontalFlipFilter

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageHorizontalFlipFragmentShaderString])) {
        return nil;
    }
    
    return self;
}

@end

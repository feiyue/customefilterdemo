//
//  GPUImageRotate90Clockwise.m
//  TestGPUImageFundationOC
//
//  Created by admin on 2020/1/10.
//  Copyright © 2020 AppTime. All rights reserved.
//

#import "GPUImageRotate90ClockwiseFilter.h"

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
NSString *const kGPUImageRotate90ClockwiseFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 void main()
 {
    //向右旋转 90
    highp vec2 textureCoordinateToUse = vec2(textureCoordinate.y, 1.0 - textureCoordinate.x);
    
    gl_FragColor = texture2D(inputImageTexture, textureCoordinateToUse);
 }
 );

#else
NSString *const kGPUImageRotate90ClockwiseFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 void main()
 {
    //向右旋转 90
    highp vec2 textureCoordinateToUse = vec2(textureCoordinate.y, 1.0 - textureCoordinate.x);
    
    gl_FragColor = texture2D(inputImageTexture, textureCoordinateToUse);
 }
 );

#endif


@interface GPUImageRotate90ClockwiseFilter ()

@end

@implementation GPUImageRotate90ClockwiseFilter

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageRotate90ClockwiseFragmentShaderString])) {
        return nil;
    }
    
    return self;
}

@end
